pragma solidity ^0.4.19;

contract Math {

    int256 num1;
    
    function Math () public{
        a = 0;
    }
    
    function getStateOfNum1() returns(int256){
        return num1;
    }
    
    function resetStateOfNum1() returns(int256){
        num1 = 0;
        return num1;
    }

    function Sum(int256 _num2) public returns(int256) {
        return num1+=_num2;
    }
    
    function Subtract(int256 _num2) public returns(int256) {
        return num1-=_num2;
    }
    
    function Multiply(int256 _num2) public returns(int256) {
        return num1*=_num2;
    }

    function Divide(int256 _num2) public returns(int256) {
        if ( _num2 != 0 ){
            return num1/=_num2;
         } else {
             return num1;
         }
    }

    function Reminder(int256 _num2) public returns(int256) {
        if(_num2 != 0){
            return num1 %= _num2;
        } else {
            return num1;
        }
    }
    
    function Power(int256 _num2) public returns(int256) {
        int256 result = 1;
        for(uint256 i = 0; i < uint256 (_num2); i++){
            result = result * num1;
        }
        num1 = result;
        result = 1;
        return num1;
    }
 
    
}