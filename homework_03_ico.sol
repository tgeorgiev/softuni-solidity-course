pragma solidity ^0.4.19;

// there is an error. short limit of time to debug
contract ICO {
    
    enum ICOStateType { Crowdsale, Open }
    
    struct InvestorType {
        address addr;
        uint tokens;
    }
    
    uint private initialDate;
    address owner;
    ICOStateType icoState;
    InvestorType public investor;
    uint contractTokens;
    
    /// Mapping of Token Balances
    mapping(address => uint) public balances;
    

    function ICO() public payable {
        initialDate = now;
        owner       = msg.sender;
        icoState    = ICOStateType.Crowdsale;
        contractTokens = 5000; // initial Contract tokens. Limited amount
    }
    
    // fallback called when ETH is sent to contract
    function() public payable {}
    
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    
    function withdraw() public onlyOwner {
        
        require(now-initialDate > 1 years); // owner withdraws after 1 year
        
        owner.transfer(this.balance);
    }
    
    function getICOBallance() public view returns(uint){
        return this.balance;
    }
    
    function buyTokens(uint _tokens) public payable {
        
        address buyer = msg.sender;
        uint cost = _tokens / 5 ether;
        
        if(now - initialDate >= 5 minutes ) {
            icoState = ICOStateType.Open;
        }
        
        require(icoState == ICOStateType.Crowdsale);
        require(buyer.balance >= cost);
        require(msg.value == cost ); //wei and ethers should match
        require(contractTokens >= _tokens);
        
        contractTokens -= _tokens; //subtract tokens from the contract
        
        balances[buyer] += _tokens;
    }
    
    function exchangeTokens(uint _tokens) public {
        
        address buyer = msg.sender;
        uint cost = _tokens / 5 ether;
        
        if(now - initialDate >= 5 minutes ) {
            icoState = ICOStateType.Open;
        }
        
        require(icoState == ICOStateType.Open);
        require(this.balance >= cost);
        require(msg.value == cost);
        require(balances[buyer] >= _tokens);
        
        contractTokens += _tokens; //add tokens to the contract
        
        balances[buyer] -= _tokens;
        
        msg.sender.transfer(cost);
    }
    
}
