pragma solidity ^0.4.18;

contract CoffeeShop {

    uint TIMEOUT_BETWEEN_PURCHASES = 2 minutes;
    uint WITHDRAW_PERIOD = 1 hours;
    uint WITHDRAW_MAX_AMOUNT = 5 ether;
    uint CAPPUCCINO_PRICE = 1 ether;
    enum SHOP_ITEMS { Late, Cappuccino, Espresso }

    uint lastTransaction;
    uint lastWithdraw;
    address owner;    
    SHOP_ITEMS item;
    
    // Mapping of ether shares of the contract.
    mapping(address => uint) pendingReturns;

    function CoffeeShop() public payable {
        lastTransaction = 0;
        lastWithdraw    = 0;
        owner           = msg.sender;
        item = SHOP_ITEMS.Cappuccino;
    }
    
    modifier notBusy {
        require(now - lastTransaction >= TIMEOUT_BETWEEN_PURCHASES);
        _;
    }
    
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    
    // fallback called when ETH is sent to contract
    function() public payable {}
    
    event LogSuccessClientOrder(address indexed from, uint tokens, SHOP_ITEMS indexed item);
    
    function checkCoffeeShopBalance() public view returns(uint) {
        return this.balance;
    }
    
    function buyCoffee() public payable notBusy returns(bool) {
    
        require(msg.value >= CAPPUCCINO_PRICE); // do not accept less for a coffee
        lastTransaction = now;
        
        //pendingReturns[msg.sender] = 0;
        uint returnAmount = pendingReturns[msg.sender];
        uint amount = msg.value;
        //uint balanceBeforeTransfer = this.balance;
              
        
        returnAmount = amount - CAPPUCCINO_PRICE;
        
        //this.transfer( CAPPUCCINO_PRICE );
        //assert(this.balance == balanceBeforeTransfer + CAPPUCCINO_PRICE);
        
        if( returnAmount > 0 ) {
            // It is important to set this to zero because the recipient
            // can call this function again as part of the receiving call
            // before `send` returns.
            pendingReturns[msg.sender] = 0;
            
            //if (!msg.sender.send(returnAmount)) {
                // No need to call throw here, just reset the amount owing
                //pendingReturns[msg.sender] = returnAmount;
            //}
            msg.sender.transfer(returnAmount);
        }
        
        
        LogSuccessClientOrder(msg.sender, CAPPUCCINO_PRICE, item);
        
        return true;
    }
    
    function withdraw() public onlyOwner returns(bool) {
        
        require(now-lastWithdraw >= WITHDRAW_PERIOD); // only one withdraw allowed in an hour
        
        uint amount = 0;
        
        amount = ( this.balance <= WITHDRAW_MAX_AMOUNT ) ? this.balance : WITHDRAW_MAX_AMOUNT;
        
        lastWithdraw = now; // stops reentrancy
        
        owner.transfer(amount);       
        
        return true;
    }
    
}
